# dnd-notebook

This is primarily meant to be a lower-footprint mechanism for taking notes about my various D&D adventures.  I had previously used Microsoft's OneNote, but the browser tab for that took approx. 175MB!  Git and Vim to the rescue!