Erlton Campaign
===============

====== link:index.html[Return to Index]

Party Information
-----------------

* Dan - Uno de Muchos - Changeling Rogue
  * Personas:
    * Uno - Gaunt male Half-Elf Rogue, notorious trickster.
    * Dos - Stunning female Elf Scholar, with renowned skill at Alchemy.
    * Tres - Mute male Orc Ruffian, with vicious scars covering his body and a famously short temper.
    * Quatro - Friendly male Dwarf Fence, knows somebody everywhere, and always "has a guy" for whatever is needed.
    * Cinco - Elderly female Human Wise-Woman, skilled with herbs and popular in some villages for her Healing Potions.
* Artie - Zane - Aasimar Druid
* Matt - Oscar Boffdan - Dwarf Tempest Cleric (NPC now)
* Sam - Henry Ashgard - Half-Elf Ranger
* Vince - Adam Randall - Human Assassin Rogue
* Mark - Proboscis - Rock Gnome Barbarian
* Bella - Aelua - Wood Elf Rogue (Assassin background)

Party Loot
----------

1. Healing Potions - 2
1. Currency:
  1. 18GP

Background - Uno
----------------

What does your character do with their free time?
Practices voice acting / imitations, or looks for dice games to make some extra
money.

What does your character love?

What does your character long for above all else? - strive for?

What does your character fear above all else?
Imprisonment, a loss of freedom

Where does your character draw the line for others?

Where does your character draw the line for themselves?

What is the one sentence that would make your character snap?

Physical description?
Depends on the persona

Mannerisms?

World view?

Pain points - what would they refuse to engage in? Or what would make them
refuse?
Just about the only thing Uno would never consider doing is betraying a friend.

What is your character’s biggest secret?

Background - Zane
-----------------

What does your character do with their free time?

What does your character love?

What does your character long for above all else? - strive for?

What does your character fear above all else?

Where does your character draw the line for others?

Where does your character draw the line for themselves?

What is the one sentence that would make your character snap?

Physical description?

Mannerisms?

World view?

Pain points - what would they refuse to engage in? Or what would make them
refuse?

What is your character’s biggest secret?


Session Notes
=============

#0 - 01/12/2020
---------------

Our party has loosely banded together for safety, leaving the same town in the
same direction.

As we head through the woods, our beautiful fall morning gets unnaturally warm,
then Zane notices that the air feels like it's about to start raining, and
raining hard.  Our helpful Rock Gnome manages to find a cave nearby, which we
settle into after a briefly checking for any kind of danger.

As Aelua goes to light her lantern, she notices a slight breeze coming from the
back of the cave.  The Rock Gnome goes to investigate and notices that there is
what appears to have been a cave-in.  He starts to pull the rocks away to open
the hole, and though one or two fall on someone's foot we eventually get a
decent opening.

Peering down the hole, we see a set of stairs (descending at a 45 degree angle)
and a strong smell of metal.  Quatro hammers in a piton, and ties off a rope to
the piton, while Proboscis grabs onto the rope and starts to explore the
staircase.  Ten or Fifteen feet down, he breaks a stair but manages to keep his
footing.  However, the noise seems to have alerted something, and we start to
hear some breathing and an almost _chittering_ type of noise.

After some deliberation, we drop a ball bearing down, and when it hits we hear
some excited chittering.  A little more deliberation, and we get lined up to go
down together.  When we get down to the bottom of the stairs, we encounter a
giant tick with a very metallic smell to it.  Our Druid recognizes that the
creature will rust any metal that it touches, and warns the party about it.

Proboscis dives down and grapples the creature, managing to pin a few of its
legs together.  Most of the party takes pot-shots and we slowly wear it down,
with Aelua landing several excellent shots.

During the combat we notice a few large purple mushrooms, and afterwards we
learn that those are poisonous so Zane burns them both with his Bonfire cantrip.
Zane also looks through a pile of clothes in the corner, and manages to find:

1. A Wand of Magic Missile
1. 2 x Healing Potions

Henry notices a draft from one corner of the room, and discovers a secret door.
He manages to find a brick that presses inward, and opens the door.  Beyond,
there is a narrow hallway with a sharp turn, and we slowly make our way through
the hallway.  About 30 feet in, a sudden *bam* as the hallway lights up, and
Adam has been struck by a bolt of electricity, and we learn that there's metal
contacts randomly throughout the hallway.

We make the rest of the way down the hallway, and hit a dead end.  It doesn't
take long though to realize that we're on the back end of a secret door.  We
carefully inspect the door, and there seems to be lights coming through the
cracks around the doorway.  Out of an abundance of caution, we head back up to
the cave to camp for the night and keep a close watch on both the cave entrance
and the stairwewll.

#1 - 02/09/2020
---------------

After a long rest, the party discusses our options and heads back down the
secret passage.  As our fearless leader (Aelua) tries to check for traps, her
hands get stuck to the door and a face appears on it.  The doors corners slam
down on her, and the party leaps into action!  After a harrowing battle (insert
more notes from audio here), we able to destroy the mimic, and find ourselves in
a room with a 10ft square raised vat, which seems almost like a large clay pipe
with a lid on it.

After Zane drops a "light" infused rock down, we learn there's a roughly 50'
drop into water.  We tie two lengths of rope together to make a 100' rope, then
secure it to the top of the pipe.  Uno stealthily descends the rope, and notices
a gathering of mushroom creatures who are staring at him.  They say "Hello," and
his torch blows out.

#2 - 03/08/2020
---------------

The torch picks back up, and the mushroom creatures invite our party to all come
down.  Uno asks a few questions, then jumps down off to the side of the stream.
The rest of the party follows, and after talking a moment or two with the off to
the side of the stream.  The rest of the party follows, and after talking a
moment or two with the off to the side of the stream.  The rest of the party
follows, and after talking a moment or two with the off to the side of the
stream.  The rest of the party follows, and after talking a moment or two with
the off to the side of the stream.  The rest of the party follows, and after
talking a moment or two with the off to the side of the stream.  The rest of the
party follows, and after talking a moment or two with the off to the side of the
stream.  The rest of the party follows, and after talking a moment or two with
the fungus, we notice a couple of the fungus expel some spores.  Some of the
party succumb to the spores and form a kind of telepathic link with the fungi.
They learn that this is a fungal garden of a wizard named "Qausitron"
(spelling is definitely wrong).

They also learn that the fungi are worried because two of their young wandered
off a couple of weeks ago, and haven't returned.  We offer to look for them, and
head down along the stream out the only exit.  As we explore single-file along
the edge of the stream, we start to see occasional flashes of light.  We stumble
upon a string dangling from ceilings, fluttering in the breeze.  After a little
longer, a spider drops down and attacks us, then quickly disappears and attacks
another.  Zane gets bit and paralyzed, but we eventually fend it off.

We are able to determine that Zane's paralysis should wear off in a few minutes,
and while we're waiting Aelua notices a skeleton in the stream, clutching an
interesting dagger and surrounded by other glittering / shimmering items.  Aelua
jumps in to the water and grabs the dagger, at which point the skeleton falls
apart and a small pouch dislodges and starts to wash away.  We are not able to
react fast enough to grab the pouch before it washes too far down the stream.

We continue down the path and come to an opening on the side.  Proboscis peers
in and sees a large, beautifully tiled room with several small raised pools of
water.  One of the pools seems to be fizzing, another is full of a pinkish
liquid, and a third has a dense fog rising up out of it (like dry ice).  The
pinkish liquid catches Henry's attention, and he takes a taste by dipping his
finger in.  He is pretty sure the liquid is safe, and possibly even helpful, so
we pour some down Zane's throat and he becomes unparalyzed.  We move along
checking out the other pools, and the fizzing one disolves the test tube and
tongs as Dos tries to check it out with her Alchemist Kit.

The fogging pool seems to be full of ice, Henry leans over and sticks his tongue
in and licks a piece of ice.  The ice burns his tongue as he pulls away.  Zane
moves on to the next pool and finds it full of wine.  He drinks a bunch of the
wine, until he ends up drunk and he stumbles to another one and sits down on the
floor next to it.  We slowly explore the various pools, passing on several
extremely offensive looks substances.  Zane also finds a pool full of treasure,
but as soon as he disturbs the surface the treasure disappears.  He sits as
still as possible, the water settles down, and then he sees the treasure again.
He's sitting on top of the treasure, tries to grab it, but it disappears again.
He tries to repeat this a few times, until Dos finally convinces him that it's
just an illusion.
